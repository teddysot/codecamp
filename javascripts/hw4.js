/**
 * ให้เขียนฟังก์ชัน checkCharacter(string) โดยคืนค่าเป็น stringโดยที่ 
ถ้า string ที่เข้ามา เป็น พิมพ์ใหญ่ทั้งหมด ให้คืนค่าเป็น string ที่มีค่าเป็น “All character are capital.”
ถ้า string ที่เข้ามา เป็น พิมพ์เล็กทั้งหมด ให้คืนค่าเป็น string ที่มีค่าเป็น “All character are small.”
ถ้า string ที่เข้ามา เป็น พิมพ์ใหญ่และพิมเล็กผสมกัน ให้คืนค่าเป็น string ที่มีค่าเป็น “All character are mix.”
 */

const checkCharacter = (input) => {
    if (input === String(input).toUpperCase()) {
        console.log("All character are capital");
    }
    else if (input === String(input).toLowerCase()) {
        console.log("All character are small");
    }
    else {
        console.log("All character are mix");
    }
}

// checkCharacter("dwAdaw")

/**
 * ให้เขียนฟังก์ชันยกกำลัง pow(a, n) โดยให้คืนค่าเป็น a ยกกำลัง n
 */

const pow = (a, n) => {
    console.log(a ** n);
}

//  pow(2,5)

/**
 * ให้ฟังก์ชัน thaiTypeof( variable ) โดยฟังก์ชันดังกล่าวจะทำหน้าที่เหมือน typeof ทุกประการแต่ จะแสดงเป็นภาษาไทย
------------------------------------
input: thaiTypeof(“Hello”)
output: “ข้อความ”
------------------------------------
input: thaiTypeof(2)
output: “ตัวเลข”
------------------------------------
input: thaiTypeof( {name: “Hello”} )
output: “ออปเจ็คต์”
 */

const thaiTypeof = (input) => {
    let type = typeof (input)
    switch (type) {
        case "number":
            console.log("ตัวเลข");
            break;
        case "string":
            console.log("ข้อความ");
            break;
        case "object":
            console.log("ออปเจ็คต์");
            break;
    }
}

// thaiTypeof({})

/**
 * ให้เขียนฟังก์ชัน evenArraySum( array ) โดยที่ฟังก์ชันนี้จะรับอาเรย์ของตัวเลข array และคืนค่าออกมาเป็นผลรวมของตัวเลขที่เป็นเลขคู่ทั้งหมด
---------------------------------------
input: [1, 3, 5, 9]
output: 0
---------------------------------------
input: [5, 8, 6, 7, 1]
output: 14
---------------------------------------
 */

const evenArraySum = (arr) => {
    let result = 0;
    for (const iterator of arr) {
        if (iterator % 2 == 0) {
            result += iterator
        }
    }

    console.log(result);
}

// evenArraySum([5,8,6,7,1])

/**
 * ให้เขียนฟังก์ชัน changeStringtoThaiDate() โดยฟังก์ชันนี้จะรับค่าวันที่ที่เป็นแบบตัวเลข และ คืนค่ามาเป็นวันที่แบบไทย แต่ถ้าวันที่ผิดพลาดหรือเดือน(กุมภาพันธ์ไม่ต้องเช็ค Leap year)ผิดพลาดให้แสดงค่าว่า “Error”
--------------------------------
input: “12-11-1996”
output: “วันที่ 12 เดือนพฤศจิกายน พ.ศ. 2539”
---------------------------------
input: “31-11-2000”
output: “Error”
--------------------------------
input: “12-13-1996”
output: “Error”
--------------------------------
 */

const changeStringToThaiDate = (input) => {
    input = String(input).split('-')
    let day = Number(input[0])
    let month = Number(input[1]) - 1
    let year = Number(input[2])
    let date = new Date(year, month + 1, 0);

    if (isNaN(day) || isNaN(month) || isNaN(year)
        || month > 11 || day > date.getDate()) {
        console.log("Error");
    }
    else {
        console.log(`วันที่${input[0]} เดือน${input[1]} พ.ศ.${Number(input[2]) + 543}`);
    }

}

// changeStringToThaiDate("31-12-1996")

/**
 * ให้เขียนฟังก์ชัน isTheSameAnagram( string1 , string2 ) โดยถ้า string ถ้าสองเป็น anagram กันให้คืนค่าาเป็น true ถ้าไม่ให้คืนเป็น false
 */

const isTheSameAnagram = (str1, str2) => {
    str1 = str1.split('').sort().join('');
    str2 = str2.split('').sort().join('');

    if (str1 === str2) {
        console.log(true);
    }
    else {
        console.log(false);
    }
}

// isTheSameAnagram("abcdefg","gfcdeba")

/**
 * ให้เขียนฟังก์ชัน numberOfSquare( height , width ) โดยให้หาจำนวนสี่จัตุรัสที่ใหญ่ที่สุดที่สามารถวางได้พอดีเช่น
-----------------------------------
input: 15, 15
output: 1
-----------------------------------
input: 20, 15
output: 12
-----------------------------------
input: 2, 8
output: 4
-----------------------------------
input: 1, 5
output: 5
 */

const gcd = (a, b) => {
    if (a == 0)
        return b;
    return gcd(b % a, a);
}

const findGCD = (arr, n) => {
    let result = arr[0];
    for (let i = 1; i < n; i++) {
        result = gcd(arr[i], result);
    }

    return result
}


const numberOfSquare = (height, width) => {
    let gcdArray = [height, width]
    let square = findGCD(gcdArray, gcdArray.length)
    let totalArea = height * width
    let squareArea = square * square

    let result = totalArea / squareArea

    console.log(result);
}

// numberOfSquare(20, 15)

/**
 * ให้เขียนฟังก์ชัน toChange( money ) โดยฟังก์ชันดังกล่าวจะแปลงเงินที่ได้รับมาเป็นเงินทอนโดยให้ใช้จำนวนแบงค์และเหรียญน้อยที่สุด เช่น
เงินจำนวน 788 บาท จะแลกเป็นเงินทอนได้
แบงค์พัน 			0 ใบ
แบงค์ห้าร้อย 		1 ใบ
แบงค์ร้อย			2 ใบ
แบงค์ห้าสิบ			1 ใบ
แบงค์ยี่สิบ			1 ใบ
เหรียญสิบ			1 เหรียญ
เหรียญห้า			1 เหรียญ
เหรียญสองบาท 		1 เหรียญ
เหรียญบาท			1 เหรียญ

Input: 788
output: “แบงค์พัน 0 ใบ / แบงค์ห้าร้อย 1 ใบ / แบงค์ร้อย 2 ใบ / แบงค์ห้าสิบ 1 ใบ / แบงค์ยี่สิบ 1 ใบ / เหรียญสิบ 1 เหรียญ / เหรียญห้า 1 เหรียญ / เหรียญสองบาท 1 เหรียญ / เหรียญบาท 1 เหรียญ”
 */

const toChange = (money) => {
    money = Math.round(money)
    let bills = [0, 0, 0, 0, 0, 0, 0, 0, 0]

    if (money >= 1000) {
        bills[0] = money / 1000
        bills[0] = Math.floor(bills[0])
        money -= (bills[0] * 1000)
    }
    if (money >= 500) {
        bills[1] = money / 500
        bills[1] = Math.floor(bills[1])
        money -= (bills[1] * 500)
    }
    if (money >= 100) {
        bills[2] = money / 100
        bills[2] = Math.floor(bills[2])
        money -= (bills[2] * 100)
    }
    if (money >= 50) {
        bills[3] = money / 50
        bills[3] = Math.floor(bills[3])
        money -= (bills[3] * 50)
    }
    if (money >= 20) {
        bills[4] = money / 20
        bills[4] = Math.floor(bills[4])
        money -= (bills[4] * 20)
    }
    if (money >= 10) {
        bills[5] = money / 10
        bills[5] = Math.floor(bills[5])
        money -= (bills[5] * 10)
    }
    if (money >= 5) {
        bills[6] = money / 5
        bills[6] = Math.floor(bills[6])
        money -= (bills[6] * 5)
    }
    if (money >= 2) {
        bills[7] = money / 2
        bills[7] = Math.floor(bills[7])
        money -= (bills[7] * 2)
    }
    if (money >= 1) {
        bills[8] = money / 1
        bills[8] = Math.floor(bills[8])
        money -= (bills[8] * 1)
    }

    if (money > 0) {
        console.log(`${money}`);
        console.log("Error");
        return;
    }

    console.log(`แบงค์พัน ${bills[0]} ใบ`);
    console.log(`แบงค์ห้าร้อย ${bills[1]} ใบ`);
    console.log(`แบงค์ร้อย ${bills[2]} ใบ`);
    console.log(`แบงค์ห้าสิบ ${bills[3]} ใบ`);
    console.log(`แบงค์ยี่สิบ ${bills[4]} ใบ`);
    console.log(`เหรียญสิบ ${bills[5]} เหรียญ`);
    console.log(`เหรียญห้า ${bills[6]} เหรียญ`);
    console.log(`เหรียญสอง ${bills[7]} เหรียญ`);
    console.log(`เหรียญหนึ่ง ${bills[8]} เหรียญ`);
}

// toChange(12345)

/**
 *  ให้เขียนฟังก์ชัน maxPossibleNumber( number ) ที่รับตัวเลขเป็นจำนวนจริงโดยคืนค่าเป็นตัวเลขที่มีค่ามากที่สุดที่เป็นไปได้ที่จำนวนหลักยังเท่าเดิม
--------------------------------------
input: 69523
output: 96532
--------------------------------------
input: 12523.97
output: 97532.21
--------------------------------------
input: 955
output: 955
--------------------------------------

 */

const maxPossibleNumber = (number) => {
    let digit = String(number).split('.')[1].length
    number = String(number).split('').sort().reverse().join('');

    console.log(Number(number) / (10 ** digit));
}

// maxPossibleNumber(12523.97)

/**
 * ให้เขียนฟังก์ชันหา squareRoot( number ) โดยเป็นการหา สแควร์รูทในรูปที่ติดรูทไว้ เช่น
50 จะได้ 5 root 2
17 จะได้ root 17
25 จะได้ 5
------‐-----------------
Input: 77
Output: root 77
------------------------
Input: 121
Output: 11
------------------------
Input: 12
Output: 2 root 3
 */

const squareRoot = (number) => {


}

// squareRoot(12)

/**
 * ให้เขียนฟังก์ชันหา dotProduct( vector1 , vector2 ) โดยวิธีหา dot product โดยการนำสมาชิกที่อยู่ตำแหน่งเดียวกันของแต่ละ vector มาคูณกันและนำทั้งหมดมาบวกกัน เช่น
[1, 2, 6, 9] dot [3, 2, 6, 3]
จะได้ (1×3) + (2×2) + (6×6) + (9×3) = 37

ในกรณีทีความยาวของ vector ไม่เท่ากันให้นำ vector ที่สั้นกว่ามาเติม 0 จนความยาวเท่ากับตัวที่ยาวกว่า เช่น
[3, 4] dot [2, 8, 9, 11]
จะได้ (3×2) + (4×8) + (0×9) + (0×11) = 38

 */

const dotProduct = (vec1, vec2) => {
    let result = 0
    for (let i = 0; i < vec1.length; i++) {
        result += (vec1[i] * vec2[i])
    }

    console.log(result);
}

// dotProduct([1, 2, 6, 9], [3, 2, 6, 3])

/**
 *  ให้เขียนฟังก์ชัน maxPairSum( array ) โดยฟังก์ชันนี้จะหาผลรวมของตัวเลขสองตัวที่บวกกันแล้วได้ค่ามากที่สุดใน array 
เช่น
--------------------
Input: [2, 5, 3, 9, 19, 3, 7, 58]
Output: 77
// 77 มาจาก 19 + 58
 */

const maxPairSum = (arr) => {
    if (arr.length < 2) {
        return arr[0];
    }
    arr = arr.sort((a, b) => b - a);

    console.log(arr[0] + arr[1]);
}

// maxPairSum([2, 5, 3, 9, 19, 3, 7, 58])

/**
 * ให้เขียนฟังก์ชัน minPairSum( array ) โดยฟังก์ชันนี้จะหาผลรวมของตัวเลขสองตัวที่บวกกันแล้วได้ค่าน้อยที่สุดใน array 
เช่น
----------------------
Input: [2, 5, 3, 9, 19, 3, 7, 58]
Output: 5
// 5 มาจาก 2 + 3

 */

const minPairSum = (arr) => {
    if (arr.length < 2) {
        return arr[0];
    }
    arr = arr.sort((a, b) => a - b);

    console.log(arr[0] + arr[1]);
}

// minPairSum([2, 5, 3, 9, 19, 3, 7, 58])

/**
 * มีเจ้ากบน้อยอยู่ตัวหนึ่ง สามารถกระโดดได้ในทุกทิศทางบนระนาบ และจะกระโดดเป็นระยะทางครั้งละ X หน่วยพอดี อยู่มาวันหนึ่ง 
 * เจ้ากบน้อยต้องการกระโดดจากจุด A ไปยังจุด B ซึ่งเป็นจุดบนระนาบ ที่ตั้งอยู่ห่างกัน Y หน่วย เจ้ากบน้อยอยากให้คุณช่วยหาว่า 
 * มันจะต้องกระโดดอย่างน้อยกี่ครั้ง จึงจะไปหยุดที่จุด B พอดี ให้เขียนฟังก์ชัน numberOfJumpFrog( speed, distance ) 
----------------------
ตัวอย่าง
input: 3, 12
output: 4 
----------------------
input: 5, 23	
output: 5
----------------------
 */

const numberOfJumpFrog = (speed, distance) => {
    console.log(Math.ceil(distance / speed));
}

// numberOfJumpFrog(5, 31)

/**
 * ในปี พศ 2945 ดอกเบี้ยเงินฝากของธนาคาร Codemy Bank ร้อยละ 2.5 โดยเป็นดอกเบี้ยทบต้นในแต่ละปี 
 * ถ้านาย Nat ฝากเงินจำนวน money บาท และฝากไปจำนวน years ปี อยากทราบว่านาย Nat จะได้เงินทั้งหมดเท่าไหร่เมื่อถอน
ให้เขียนฟังก์ชัน calculateInterest( money , years ) โดยคืนค่าเป็นจำนวนเงินที่นาย Nat จะได้หลังจากถอนเงิน
----------------------------------------
input: 15126, 14
output: 21372.64201604918
----------------------------------------
input: 100000, 1
output: 102500
 */

const calculateInterest = (money, years) => {
    for (let i = 0; i < years; i++) {
        money += ((money * 2.5) / 100)
    }

    console.log(money);
}

// calculateInterest(100000, 1)