/*
ให้เขียนโปรแกรมที่รับค่า a และ b จาก prompt อย่างละครั้ง 
และให้เช็คว่า a / b เป็นเศษส่วนอย่างต่ำไหม ถ้าเป็นให้แสดง 
alert(a / b) ออกมา ถ้าไม่เป็นเศษส่วนอย่างต่ำ 
ให้หาเศษส่วนอย่างต่ำของ a และ b ก่อน หลังจากนั้น alert(a / b) 
ที่เป็นเศษส่วนอย่างต่ำแล้วออกมา
*/

const irrFraction = () => {
    let a = Number(prompt());
    let b = Number(prompt());

    let gcdResult = gcd(a, b);

    let numerator = a / gcdResult;
    let denominator = b / gcdResult;

    alert(`เศษส่วนอย่างต่ำของ${a}/${b} คือ ${numerator}/${denominator}`)
}

const gcd = (a, b) => {

    // Divides by 0
    if (a == 0) {
        return b;
    }
    if (b == 0) {
        return a;
    }

    // base case
    if (a == b) {
        return a;
    }

    // a greater than b
    if (a > b) {
        return gcd(a - b, b);
    }

    return gcd(a, b - a);
}

//irrFraction() // Start Function

//////////////////////////////////////////////////////////////

/*
ให้เขียนโปรแกรมที่บวกเลข 160 + 162 + 164 + … + 2048 แล้ว alert คำตอบออกมา
*/

const addingNumber = () => {

    let result = 0;
    for (let i = 160; i <= 2048; i += 2) {
        result += i;
    }

    alert(`ผลรวมคือ${result}`);
}
// addingNumber(); // Start Funciton

//////////////////////////////////////////////////////////////

/*
ให้เขียนโปรแกรมที่บวกเลข 163 + 167 + 171 + … + 815 แล้ว alert คำตอบออกมา
*/

const addingNumber2 = () => {

    let result = 0;
    for (let i = 163; i <= 815; i += 4) {
        result += i;
    }

    alert(`ผลรวมคือ${result}`);
}
// addingNumber2(); // Start Funciton

//////////////////////////////////////////////////////////////

/*
ให้เขียนโปรแกรมที่แสดงลำดับตัวที่ n ของลำดับ Fibonacci
ลำดับ Fibonacci คือ
0, 1, 1, 2, 3, 5, 8, 13, …
(ผลลัพธ์เกิดจากการบวกกันของสองตัวก่อนหน้า)
*/


const fibonacci = () => {

    let n = Number(prompt(`ตำแหน่งที่ต้องการในFibonacci`));
    if (n == NaN || n == null || n == undefined) {
        alert(`Input Error!`);
    }
    else {
        if (n == 1) {
            alert(0);
        }
        else if (n == 2) {
            alert(1);
        }
        else {
            let resultPrev1 = 0;
            let resultPrev2 = 1;
            let result = 0;
            for (let i = 3; i <= n; i++) {
                result = resultPrev1 + resultPrev2;
                resultPrev1 = resultPrev2;
                resultPrev2 = result;
            }

            alert(result);
        }
    }
}

// fibonacci(); // Start Function

//////////////////////////////////////////////////////////////

/*
ให้เขียนโปรแกรมที่หาผลลัพธ์ของ 1 - 2 + 3 - 4 + 5 - 6 + 7 - … + 18953 - 18954 แล้วแสดง alert คำตอบออกมา
*/

const altNumber = () => {

    let result = 0;
    let alt = 1;
    for (let i = 1; i <= 18954; i++) {
        switch (alt) {
            case 0:
                result -= i;
                alt = 1;
                break;
            case 1:
                result += i;
                alt = 0;
                break;

            default:
                break;
        }
    }

    alert(result);
}

// altNumber(); // Start Function

//////////////////////////////////////////////////////////////

/**
 * ให้เขียนโปรแกรมที่หาผลลัพธ์ของ 1x2 + 2x3 + 3x4 + … + 87x88 แล้ว alert ผลลัพธ์ออกมา
 */

const compNumber = () => {
    let result = 0;
    for (let i = 1; i <= 87; i++) {
        result += (i * i + 1);
    }
}

// compNumber(); // Start Function

//////////////////////////////////////////////////////////////

/**
 * ให้เขียนโปรแกรมที่หาผลลัพธ์ของ 1x2x3 + 2x3x4 + 3x4x5 + … + 87x88x89 แล้ว alert ผลลัพธ์ออกมา
 */

const compNumber2 = () => {
    let result = 0;
    for (let i = 1; i <= 87; i++) {
        result += (i * i + 1 * i + 2);
    }
}

// compNumber2(); // Start Function

//////////////////////////////////////////////////////////////

/**
 * ให้เขียนโปรแกรมที่หาผลลัพธ์ของ 1x3x5 + 3x5x7 + 5x7x9 + … + 155x157x159 แล้ว alert ผลลัพธ์ออกมา
 */

const compNumber3 = () => {
    let result = 0;
    for (let i = 1; i <= 155; i += 2) {
        result += (i * i + 2 * i + 4);
    }
}

// compNumber3(); // Start Function

//////////////////////////////////////////////////////////////

/**
 * ให้รับค่าจาก prompt มาเรื่อย ๆ จนกระทั่ง เจอคำว่า stop แล้วให้แสดงตัวเลขที่น้อยที่สุดออกมา
 */

const minInfPrompt = () => {
    let result;
    let input;

    while (input != `stop`) {
        input = Number(prompt())
        if (input != NaN || input != undefined) {
            if (result < input) {
                continue;
            }
            else {
                result = input;
            }
        }
    }

    alert(result)
}

// minInfPrompt(); // Start Function

//////////////////////////////////////////////////////////////

/**
 * ให้รับค่าจาก prompt มาเรื่อย ๆ จนกระทั่ง เจอคำว่า stop แล้วให้แสดงตัวเลขที่น้อยที่สุดออกมา 3 อันดับแรก
 */

const minInfPrompt2 = () => {
    let result = [undefined, undefined, undefined];
    let input;

    while (input != `stop`) {
        input = Number(prompt())
        if (input != NaN || input != undefined) {
            let tmp;
            for (let i = result.length - 1; i > 0; i--) {
                if (result[i] < input) {
                    tmp = result[i];
                    result[i] = input;

                    if (i < 2) {
                        result[i + 1] = tmp;;
                    }
                }
            }
        }
    }

    alert(`${result[0]} ${result[1]} ${result[2]}`);
}

// minInfPrompt2(); // Start Function

//////////////////////////////////////////////////////////////

/**
 * ให้รับค่าจาก prompt มาเรื่อย ๆ จนกระทั่ง เจอคำว่า stop แล้วให้แสดงตัวเลขที่น้อยที่สุดออกมา 3 อันดับแรก
 */

const minInfPrompt3 = () => {
    let result = [undefined, undefined, undefined];
    let input;

    while (input != `stop`) {
        input = Number(prompt())
        if (input != NaN || input != undefined) {
            let tmp;
            for (let i = result.length - 1; i > 0; i--) {
                if (result[i] < input) {
                    tmp = result[i];
                    result[i] = input;

                    if (i < 2) {
                        result[i + 1] = tmp;;
                    }
                }
            }
        }
    }

    alert(`${result[0]} ${result[1]} ${result[2]}`);
}

// minInfPrompt3(); // Start Function

//////////////////////////////////////////////////////////////

/**
 * ให้รับค่าจาก prompt มาเรื่อย ๆ จนกระทั่ง เจอคำว่า stop แล้วให้แสดงตัวเลขที่น้อยที่สุด และ มากที่สุดออกมา
 */

const minMaxInfPrompt = () => {
    let resultMin;
    let resultMax;
    let input;

    input = Number(prompt())
    if (input != NaN || input != undefined) {
        resultMin = input;
        resultMax = input;

        while (input != `stop`) {
            input = Number(prompt())
            if (input != NaN || input != undefined) {
                if (input < resultMin) {
                    resultMin = input;
                    continue;
                }

                if (input > resultMax) {
                    resultMax = input;
                    continue;
                }
            }
        }
    }

    alert(`Min:${resultMin} Max:${resultMax}`);
}

// minMaxInfPrompt(); // Start Function

//////////////////////////////////////////////////////////////

/**
 * ให้รับค่าจาก prompt มาเรื่อย ๆ จนกระทั่ง เจอคำว่า stop แล้วให้แสดงตัวเลขที่น้อยที่สุด และ มากที่สุดออกมา
 */

const minMaxInfPrompt2 = () => {
    let resultMin;
    let resultMax;
    let input;

    input = Number(prompt())
    if (input != NaN || input != undefined) {
        resultMin = input;
        resultMax = input;

        while (input != `stop`) {
            input = Number(prompt())
            if (input != NaN || input != undefined) {
                if (input < resultMin) {
                    resultMin = input;
                    continue;
                }

                if (input > resultMax) {
                    resultMax = input;
                    continue;
                }
            }
        }
    }

    alert(`Result:${resultMax - resultMin}`);
}

// minMaxInfPrompt2(); // Start Function

//////////////////////////////////////////////////////////////

/**
 * ให้รับค่าจาก prompt มาเรื่อย ๆ จนกระทั่ง เจอคำว่า stop แล้วให้แสดงตัวเลขที่น้อยที่สุด และ มากที่สุดออกมา
 */

const minMaxAvgInfPrompt = () => {
    let resultCount = 0;
    let resultSum = 0;
    let input;

    while (input != `stop`) {
        input = Number(prompt())
        if (input != NaN || input != undefined) {
            resultCount++;
            resultSum += input;
        }
    }

    alert(`Avg:${resultSum / resultCount}`);
}

// minMaxAvgInfPrompt(); // Start Function

//////////////////////////////////////////////////////////////

/**
 * ให้รับค่าจาก prompt ที่เป็น Number เข้ามา และให้ทำการสลับตัวเลขจากหลังไปหน้าและ alert ค่านั้นออกมา ถ้าเป็นลบให้คงค่าลบนั้นไว้ด้วย
 */

const reversePrompt = () => {
    let result = 0;
    let input;

    input = Number(prompt("Input number to reverse"))
    if (input != NaN || input != undefined) {
        while (input != 0) {
            let rem = input % 10;
            result = result * 10 + rem;
            input /= 10;
            input = Math.floor(input);
        }
        alert(`Reverse:${result}`);
    }
    else {
        alert(`Invalid Input!`);
    }

}

//reversePrompt(); // Start Function

//////////////////////////////////////////////////////////////