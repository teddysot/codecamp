/**
 * 1. หา Prime Number
 */

const checkPrimeNumber = () => {
    let primeNumber = []

    for (let i = 2; i < 100; i++) {
        if (i == 2 || i == 3) {
            primeNumber.push(i);
        }
        for (let j = 2; j * j <= i; j++) {
            if (i % j == 0) {
                break;
            }
            else if (j + 1 > Math.sqrt(i)) {
                primeNumber.push(i);
            }
        }
    }

    console.log(primeNumber);
}

// checkPrimeNumber();

/**
 * 2. หา Prime Number 2
 * ให้เขียนโปรแกรมที่รับ input เป็นตัวแปรชื่อ n โดยเมื่อรับมาแล้วให้คืนค่าออกมาเป็น List ของจำนวนเฉพาะที่มีค่าไม่มากกว่า n
 */

const checkPrimeNumber2 = (input) => {
    let primeNumber = []
    for (let i = 2; i < input; i++) {
        if (i == 2 || i == 3) {
            primeNumber.push(i);
        }
        for (let j = 2; j * j <= i; j++) {
            if (i % j == 0) {
                break;
            }
            else if (j + 1 > Math.sqrt(i)) {
                primeNumber.push(i);
            }
        }
    }
    console.dir(primeNumber, { 'maxArrayLength': null });
}

// checkPrimeNumber2(942);

/**
 * 3. หา Prime Number 3
 * ให้เขียนโปรแกรมที่รับ input n และแสดง Array ของจำนวนเฉพาะ n ตัวแรก
 */

/**
* 4. หา Prime Number 4
* ให้เขียนโปรแกรมที่หาผลบวกจำนวนเฉพาะ n ตัวแรก
*/

const sumPrimeNumber = (input) => {
    let primeNumber = 0
    for (let i = 2; i <= input; i++) {
        if (i == 2 || i == 3) {
            primeNumber += i
        }
        for (let j = 2; j * j <= i; j++) {
            if (i % j == 0) {
                break;
            }
            else if (j + 1 > Math.sqrt(i)) {
                primeNumber += i
            }
        }
    }
    console.dir(`Sum of prime number between ${input} is ${primeNumber}`, { 'maxArrayLength': null });
}

// sumPrimeNumber(5);

/**
* 5.   ลำดับ
* ให้เขียนโปรแกรมที่รับค่า n เข้ามาและให้บวกค่าของลำดับต่อไปนี้
*/

const sumOrderNumber = (input) => {
    let sum = 0;
    for (let i = 1; i <= input; i++) {
        for (let j = 0; j < i; j++) {
            sum += i;
        }
    }

    console.log(sum);
}

// sumOrderNumber(4);

/**
* 6.	 Prime Number - 101
* ให้เขียนโปรแกรมหาผลบวกลำดับต่อไปนี้
1 - 2 - 3 + 4 - 5 + 6 - 7 + 8 + 9 + 10 - 11 + ... (จำนวนที่เป็นจำนวนเฉพาะให้ติดลบ)
*/

const sumPrimeNumber6 = (input) => {
    let isPrime = false;
    let result = 0;
    for (let i = 1; i <= input; i++) {
        if (i == 2 || i == 3) {
            result -= i;
        }
        else {
            for (let j = 2; j * j <= i; j++) {
                if (i % j == 0) {
                    break;
                }
                else if (j + 1 > Math.sqrt(i)) {
                    isPrime = true;
                }
            }

            if (isPrime) {
                result -= i;
                isPrime = false;
            }
            else {
                result += i;
            }
        }
    }
    console.log(result);
}

// sumPrimeNumber6(101);

/**
* 7.	 ห.ร.ม.
*/

// https://www.geeksforgeeks.org/cpp-program-for-gcd-of-more-than-two-or-array-numbers/
const gcd = (a, b) => {
    if (a == 0)
        return b;
    return gcd(b % a, a);
}

const findGCD = (arr, n) => {
    let result = arr[0];
    for (let i = 1; i < n; i++) {
        result = gcd(arr[i], result);
    }

    console.log(result);
}

let gcdArray = [6, 8, 16]
// findGCD(gcdArray, gcdArray.length)

/**
* 8.	 ค.ร.น
*/

const findLCM = (arr, n) => {
    let result = arr[0];
    for (let i = 1; i < n; i++) {
        result = ((arr[i] * result) / (gcd(arr[i], result)))
    }

    console.log(result);
}

let lcmArray = [2, 3, 5, 7];
// findLCM(lcmArray, lcmArray.length);

/**
* 9.	 n!
*/

const factorial = (n) => {
    let result = 1
    for (let i = 1; i < n; i++) {
        result *= i + 1
    }

    console.log(result);
    return result;
}

// factorial(5);

/**
 * 10. ให้เขียน function ที่รับ List ของ ตัวเลข และ รับค่า boolean
    ถ้า boolean มีค่าเป็น true เรียงลำดับตัวเลขจากน้อยไปหามาก
    ถ้า boolean เป็น false เรียงลำดับตัวเลขจากมากไปน้อย
 */

const sortNumber = (arr, reverse) => {

    arr = arr.sort();
    if (reverse == true) {
        arr = arr.reverse();
    }

    console.log(arr);
}

let sortArray = [3, 5, 1, 2, 4]
// sortNumber(sortArray, true)

/**
 * 11. 
 */

// https://www.guru99.com/quicksort-in-javascript.html
const swap = (items, leftIndex, rightIndex) => {
    let temp = items[leftIndex];
    items[leftIndex] = items[rightIndex];
    items[rightIndex] = temp;
}

const partition = (items, left, right) => {
    let pivot = items[Math.floor((right + left) / 2)], //middle element
        i = left, //left pointer
        j = right; //right pointer
    while (i <= j) {
        while (items[i] < pivot) {
            i++;
        }
        while (items[j] > pivot) {
            j--;
        }
        if (i <= j) {
            swap(items, i, j); //swap two elements
            i++;
            j--;
        }
    }
    return i;
}

const quickSort = (items, left, right) => {
    let index;
    if (items.length > 1) {
        index = partition(items, left, right); //index returned from partition
        if (left < index - 1) { //more elements on the left side of the pivot
            quickSort(items, left, index - 1);
        }
        if (index < right) { //more elements on the right side of the pivot
            quickSort(items, index, right);
        }
    }
}

const reverseSort = (arr) => {

    let start = 0;
    let end = arr.length - 1;

    while (start < end) {
        let temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
        start++;
        end--;
    }
}

const sortNumber2 = (arr, reverse) => {

    quickSort(arr, 0, arr.length - 1);
    if (reverse == true) {
        reverseSort(arr);
    }

    console.log(arr);
}

// sortNumber2(sortArray, true);

/**
 * 12. ถ้าจำนวนนับที่น้อยกว่า 16 ที่เป็นพหุคูณของ 3 หรือ 5 เท่ากับ 3, 5, 6, 9, 10, 12 
 * และ 15 โดยมีผลรวมเท่ากับ 60 แล้ว (3 + 5 + 6 + 9 + 10 + 12 + 15 = 60)
 * จงหาผลรวมของจำนวนนับ ที่เป็นพหุคูณของ 3 หรือ 5 ที่มีค่าน้อยกว่า 1000
 */

const sumCountNumber = (input) => {
    let i = 3;
    let j = 5;
    let arr = new Set();
    for (let i = 3; i < input; i += 3) {
        arr.add(i);
    }

    for (let i = 5; i < input; i += 5) {
        arr.add(i);
    }

    arr = Array.from(arr);

    let result = arr.reduce((acc, item) => acc + item, 0)

    console.log(result);
}

// sumCountNumber(1000);

/**
 * 13.	ผลรวมของเลขโดด
 * หาผลรวมของเลขที่ใส่เข้ามา
 * เช่น 130,120
 * จะได้ 1+3+0+1+2+0 = 7
 */

const sumDigit = (input) => {
    input = String(input);
    let result = 0;

    for (const iterator of input) {
        result += Number(iterator);
    }

    return result;
}

// sumDigit(130120)

/**
 * 14.	ผลรวมของเลขโดดของ n!
 * 
 */

const sumDigitFactorial = (input) => {
    let facNum = factorial(input);
    sumDigit(facNum);
}

// sumDigitFactorial(6)

/**
 * 15.	ผลรวมของเลขโดดหลายจำนวน
 * จงหาจำนวนเลขโดดตั้งแต่ 1 ถึง 4,129,980
 */

const sumDigit2 = () => {
    for (let i = 1; i <= 4129980; i++) {
        console.dir(`${i} : ${sumDigit(i)}`, { 'maxArrayLength' : null });
    }
}