const clickHandler = (e) => {
    alert(e.target.name);
}
const submitHandler = () => {

}

// const btnClick = document.getElementById('btnClick');
// btnClick.onclick = () => {
//     alert("Alert DOM Prop");
// };

// const formTest = document.getElementById('formTest');
// formTest.onsubmit = () => {
//     alert("Submit")
// }

const btnClick = document.getElementById('btnClick');
btnClick.addEventListener('click', clickHandler);
// btnClick.removeEventListener('click', clickHandler); 

const addClick = document.getElementById('todo-add');
let listCount = 0;

const editBtn = () => {

}

const removeBtn = (event) => {
    const todoItem = event.target.parentElement;
    todoItem.remove();
}

const completeTodo = () => {

}

addClick.addEventListener('click', addTodo);

const addTodo = (event) => {
    this.preventDefault;

    const todoItem = document.createElement(`div`);
    todoItem.classList.add('todo-item');

    const todoItemText = document.createElement(`li`);
    todoItemText.innerHTML = todoInput.value;
    todoItemText.addEventListener('click', completeTodo);

    const delButton = document.createElement('button');
    delButton.innerHTML = "Delete";
    delButton.classList.add('todo-delete-button');
    delButton.addEventListener('click', removeBtn);

    const editButton = document.createElement('button');
    editButton.innerHTML = "Edit";
    editButton.classList.add('todo-edit-button');
    editButton.addEventListener('click', editBtn);


    todoItem.appendChild(todoItemText);
    todoItem.appendChild(editButton);
    todoItem.appendChild(delButton);

    todoContainer.appendChild(todoItem);

    todoInput.value = '';
}

const provinceObj = {
    bangkok: ['Ratchathewi', 'Pathumwan'],
    nonthaburi: ['Mueang', 'Pakgret']
};

const changeProvince = (e) => {
    const selectedProvince = e.target.value;
    const districtArr = provinceObj[selectedProvince].map(ele => `<option>${ele}</option>`);
    document.getElementById('district').innerHTML = districtArr.join('');
};

let show = true;
const toggle = () => {
    if (show) {
        document.getElementById('toggleBtn').innerHTML = 'Show';
        document.getElementById('text').hidden = true;
    } else {
        document.getElementById('toggleBtn').innerHTML = 'Hide';
        document.getElementById('text').hidden = false;
    }
    show = !show;
}