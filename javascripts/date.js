const printLog = (value) => {
    console.log(value);
    return value + 100;
}

// let gg = printLog(123);

var a = 1
a = "1"
// console.log(`Result: ${gg}`);

const Allowing = () => {
    let IsAllow = confirm(`คุณอายุมากกว่า 18 ปี หรือไม่`);
}

const Prompting = () => {
    let person = prompt("Please enter your name", "Harry Potter");
}

let user = {
    name: `Earth`,
    age: 23
}

function Accumulator(startingValue) {
    this.value = Number(startingValue);

    this.read = () => {
        this.value += Number(prompt("Enter adding value:"));

        return this;
    };
}

// let persons = [`John`, `George`, `GGWP`];

// let results = persons.map((person) => {
//     return `${person}-Name`;
// })

let persons = [
    { name: `John`, age: 15 },
    { name: `George`, age: 20 },
    { name: `GGWP`, age: 45 },
    { name: `Raz`, age: 70 },
]

let results = persons.filter((person) => {
    return person.age > 16;
})

// Loop through array sum item.age to acc
const totalPersonsAge = persons.reduce((acc, item) => {
    return acc + item.age;
}, 0)

// console.log(totalPersonsAge);

const a1 = [1, 2, 30, 400];
const array1 = a1.map(a => a * 2);

const a2 = [1, 2, 3, 4];
const array2 = a2.map(a => a.toString())

const a3 = [1, '1', 2, {}];
const array3 = a3.map(a => typeof (a))

const a4 = ["apple", "banana", "orange"];
const array4 = a4.map(a => a.toUpperCase())

const a5 = [
    { name: `apple`, age: 14 },
    { name: `watermelon`, age: 18 },
    { name: `watermelon`, age: 32 },
]

const array5 = a5.map(a => a.name)

const a6 = [
    { name: `apple`, age: 14 },
    { name: `watermelon`, age: 18 },
    { name: `watermelon`, age: 32 },
]

const array6 = a6.map(a => a.age)

const a7 = [
    { name: "apple", surname: "London" },
    { name: "banana", surname: "Bangkok" },
    { name: "watermelon", surname: "Singapore" },
]

const array7 = a7.map(a => a.name + ' ' + a.surname);

const a8 = [1, 3, 4, 5, 6, 7, 8];
const array8 = a8.map(a => a % 2 == 0 ? 'even' : 'odd')

const a9 = [1, -3, 2, 8, -4, 5];
const array9 = a9.map(a => Math.abs(a));

const a10 = [100, 200.25, 300.84, 400.3];
const array10 = a10.map(a => a.toFixed(2).toString());

const a11 = [
    { name: `apple`, birth: `2000-01-01` },
    { name: `banana`, birth: `1990-10-01` },
    { name: `watermelon`, birth: `1985-12-01` },
]

const array11 = a11.map((a) => {
    let date_array = a.birth.split(`-`);
    let MILLISECONDS_IN_A_YEAR = 1000 * 60 * 60 * 24 * 365;
    a.age = Math.floor((new Date() - new Date(date_array[0], date_array[1], date_array[2])) / (MILLISECONDS_IN_A_YEAR));
    return a;
});

const a12 = [
    { name: `apple`, birth: `2000-01-01` },
    { name: `banana`, birth: `1990-10-01` },
    { name: `watermelon`, birth: `1985-12-01` },
];

const array12 = a12.map((a) => {

    return `<tr> <td>${a.name}</td><td>${a.birth}</td> </tr>`;
})


const a21 = [1, 2, 30, 400];
const array21 = a21.filter(a => a > 10)

const a22 = [1, 2, 3, 4];
const array22 = a22.filter(a => a % 2 == 1);

const a23 = [1, '1', 2, {}];
const array23 = a23.filter((a) => {
    if (typeof (a) == "number") {
        return a
    }
})

const a24 = ["apple", "banana", "orange", "pineapple", "watermelon"];
const array24 = a24.filter(a => a.length > 6)

const a25 = [
    { name: "apple", age: 14 },
    { name: "banana", age: 18 },
    { name: "watermelon", age: 32 },
    { name: "pineapple", age: 16 },
    { name: "peach", age: 24 },
]

const array25 = a25.filter(a => a.age < 18)

const a26 = [
    { name: "apple", age: 14 },
    { name: "banana", age: 18 },
    { name: "watermelon", age: 32 },
    { name: "pineapple", age: 16 },
    { name: "peach", age: 24 },
]

const array26 = a26.filter(a => a.age != 32)

const a27 = [1, -3, 2, 8, -4, 5]
const array27 = a27.filter(a => a > 0)

const a28 = [1, 3, 4, 5, 6, 7, 8]
const array28 = a28.filter(a => a % 3 == 0)

const a29 = ["peach", 1, -3, "2", {}, []]
const array29 = a29.filter(a => typeof (a) == 'string')

const a30 = ["APPLE", "appLE", "PEACH", "PEach"]
const array30 = a30.filter(a => a == a.toUpperCase())

const a31 = [
    { name: "apple", birth: "2001-01-01" },
    { name: "banana", birth: "1990-10-10" },
    { name: "watermelon", birth: "1985-12-30" },
    { name: "peach", birth: "2002-10-13" },
]

const array31 = a31.filter((a) => {
    let month = a.birth.split('-')[1];

    return month == 10;
})

const array32 = a31.filter((a) => {
    let year = a.birth.split('-')[0];

    return year < 2000;
})

// const loopList = () => {
//     let input = ''
//     while (input !== null) {
//         input = prompt("Input new list");

//         if (input === null) {
//             return;
//         }

//         list.insertAdjacentHTML("beforeend", `<li>${input}</li>`);
//     }
// }

// loopList();

let data = {
    Fish: {
        trout: {},
        salmon: {}
    },

    Tree: {
        Huge: {
            sequoia: {},
            oak: {}
        },
        Flowering: {
            appletree: {},
            magnolia: {}
        }
    }
};

// let createTree = (id, content) => {
//     let arrayContent = Object.entries(content);

//     for (let i = 0; i < arrayContent.length; i++) {
//         id.insertAdjacentHTML("beforeend", `<li id="${i}0">${arrayContent[i][0]}</li>`)

//         let arrayContent2 = Object.entries(arrayContent[i][1]);

//         console.log(arrayContent2);

//         if (arrayContent2.length != 0) {
//             let inList = document.getElementById(`${i}0`);
//             inList.insertAdjacentHTML("beforeend", `<ul id=${i}0-u></ul>`);

//             let insideSubList = document.getElementById(`${i}0-u`);

//             for (let j = 0; j < arrayContent2.length; j++) {
//                 console.log(insideSubList);
//                 insideSubList.insertAdjacentHTML("beforeend", `<li>${arrayContent2[j][0]}</li>`)
//             }
//         }
//     }
// }
// let container = document.getElementById(`container`);
// createTree(container, data); // creates the tree in the container

let createTree = (obj) => {
    let container = document.body;
    container.append(createTreeText(obj))
}

let createTreeText = (obj) => {
    if (Object.keys(obj).length == 0) return;

    let ul = document.createElement(`ul`);

    for (const key in obj) {
        let li = document.createElement(`li`);

        li.innerHTML = key;

        let childrenUl = createTreeText(obj[key]);
        if (childrenUl) {
            li.append(childrenUl);
        }

        ul.append(li);
    }

    return ul;
}

let addCount = () => {
    let list = document.getElementsByTagName(`li`);

    for (const li of list) {
        let descendantsCount = li.getElementsByTagName(`li`).length;
        if (descendantsCount == false) continue;

        li.firstChild.data += `[${descendantsCount}]`;
    }
}

// let getDay = (date) => {
//     let day = date.getDay();
//     if (day === 0) {
//         day = 7;
//     }

//     return day - 1;
// }

let createCalendar = (elem, year, month) => {

    let monthIdx = month - 1;
    let theDay = new Date(year, monthIdx);

    let table = `<table><tr><th>MO</th><th>TU</th>
<th>WE</th><th>TH</th><th>FR</th><th>SA</th><th>SU</th></tr><tr>`

    for (let i = 0; i < getDay(theDay); i++) {
        table += `<td></td>`;
    }

    while (theDay.getMonth() === monthIdx) {
        table += `<td>${theDay.getDate()}</td>`;

        if (getDay(theDay) === 6) {
            table += `</tr<tr>`
        }

        theDay.setDate(theDay.getDate() + 1);
    }

    if (getDay(theDay) !== 0) {
        for (let i = getDay(theDay); i < 7; i++) {
            table += `<td></td>`;
        }
    }

    table += `</tr></table>`;

    elem.innerHTML = table;
}

// createCalendar(calendar, 2012, 9);

// function createCalendar(elem, year, month) {
//     let monIdx = month - 1;
//     let theDay = new Date(year, monIdx);

//     let table = `<table><tr><th>MO</th><th>TU</th>
//       <th>WE</th><th>TH</th><th>FR</th><th>SA</th><th>SU</th></tr><tr>`

//     for (let i = 0; i < getDay(theDay); i++) {
//         table += `<td></td>`;
//     }

//     while (theDay.getMonth() === monIdx) {
//         table += `<td>${theDay.getDate()}</td>`;

//         if (getDay(theDay) === 6) {
//             table += `</tr><tr>`;
//         }

//         theDay.setDate(theDay.getDate() + 1);
//     }

//     if (getDay(theDay) !== 0) {
//         for (let i = getDay(theDay); i < 7; i++) {
//             table += '<td></td>';
//         }
//     }
//     table += `</tr></table>`;

//     elem.innerHTML = table;
// }

function getDay(date) {
    let day = date.getDay();
    if (day === 0) day = 7;
    return day - 1;
}

createCalendar(calendar, 2012, 9);