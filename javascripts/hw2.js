/**
 * 1
 */

const draw = (n) => {
    n = Math.floor(n);
    let result = ``;
    for (let i = 0; i < n; i++) {
        result += `*`;
    }

    alert(result);
}
// draw(prompt()); // Start Funciton

//////////////////////////////////////////////////////////////
/**
 * 2
 */

const draw2 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;
    for (let i = 0; i < n; i++) {
        result += `*`;
    }

    for (let j = 0; j < n; j++) {
        resultSum += result + `\n`;
    }

    alert(resultSum);
}
// draw2(prompt()); // Start Funciton

//////////////////////////////////////////////////////////////
/**
 * 3
 */

const draw3 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;
    for (let i = 0; i < n; i++) {
        for (let j = 1; j <= n; j++) {
            result += `${j}`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
//draw3(prompt()); // Start Funciton

//////////////////////////////////////////////////////////////
/**
 * 4
 */

const draw4 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;
    for (let i = 1; i <= n; i++) {
        for (let j = 0; j < n; j++) {
            result += `${i}`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
//draw4(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 5
 */

const draw5 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;
    for (let i = n; i >= 1; i--) {
        for (let j = 0; j < n; j++) {
            result += `${i}`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
// draw5(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 6
 */

const draw6 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;
    let count = 0;
    let countSet = n;
    for (let i = 0; i < n; i++) {
        for (let j = count + 1; j <= countSet; j++) {
            result += `${j}`;
            count++;
        }
        resultSum += result + `\n`;
        result = ``;
        countSet += n;
    }

    alert(resultSum);
}
// draw6(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 7
 */

const draw7 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;
    let countSet = n * n;
    let count = countSet - (countSet / n);
    for (let i = 0; i < n; i++) {
        for (let j = countSet; j > count; j--) {
            result += `${j}`;
        }
        resultSum += result + `\n`;
        result = ``;
        countSet = count;
        count -= n
    }

    alert(resultSum);
}
//  draw7(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 8
 */

const draw8 = (n) => {
    n = Math.floor(n);
    let result = ``;

    for (let i = 0; i < n; i++) {
        result += i * 2 + `\n`;
    }

    alert(result);
}
// draw8(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 9
 */

const draw9 = (n) => {
    n = Math.floor(n);
    let result = ``;

    for (let i = 1; i <= n; i++) {
        result += 2 * i + `\n`;
    }

    alert(result);
}
// draw9(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 10
 */

const draw10 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;
    let count = 1;
    let countSet = 0;

    for (let i = 1; i <= n; i++) {
        countSet += n;
        for (let j = count; j <= countSet; j += count) {
            result += j;
        }
        resultSum += result + `\n`;
        result = ``;
        count++;
    }

    alert(resultSum);
}
// draw10(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 11
 */

const draw11 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;

    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n; j++) {
            if (i == j) {
                result += `_`;
                continue;
            }
            result += `*`;
        }

        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
// draw11(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 12
 */

const draw12 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;

    for (let i = n; i > 0; i--) {
        for (let j = 0; j < n; j++) {
            if (j == i - 1) {
                result += `_`;
                continue;
            }
            result += `*`;
        }

        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
// draw12(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 13
 */

const draw13 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;

    for (let i = 1; i <= n; i++) {

        for (let k = 0; k < i; k++) {
            result += `*`;
        }

        for (let j = n - i; j >= 1; j--) {
            result += `_`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
// draw13(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 14
 */

const draw14 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;

    for (let i = n; i >= 1; i--) {

        for (let k = 0; k < i; k++) {
            result += `*`;
        }

        for (let j = n - i; j >= 1; j--) {
            result += `_`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
//  draw14(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 15
 */

const draw15 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;

    for (let i = 1; i <= n; i++) {

        for (let k = 0; k < i; k++) {
            result += `*`;
        }

        for (let j = n - i; j >= 1; j--) {
            result += `_`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    for (let i = n - 1; i >= 1; i--) {

        for (let k = 0; k < i; k++) {
            result += `*`;
        }

        for (let j = n - i; j >= 1; j--) {
            result += `_`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
// draw15(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 17
 */

const draw17 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;

    for (let i = 1; i <= n; i++) {

        for (let k = 0; k < n - i; k++) {
            result += `-`;
        }

        for (let j = n; j > n - i; j--) {
            result += `*`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
//  draw17(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 18
 */

const draw18 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;

    for (let i = n; i >= 1; i--) {

        for (let k = 0; k < n - i; k++) {
            result += `_`;
        }

        for (let j = n; j > n - i; j--) {
            result += `*`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
//  draw18(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 19
 */

const draw19 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;

    for (let i = 1; i < n; i++) {

        for (let k = 0; k < n - i; k++) {
            result += `-`;
        }

        for (let j = n; j > n - i; j--) {
            result += `*`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    for (let i = n; i >= 1; i--) {

        for (let k = 0; k < n - i; k++) {
            result += `_`;
        }

        for (let j = n; j > n - i; j--) {
            result += `*`;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
//  draw19(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 20
 */

const draw20 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;
    let count = 1;

    for (let i = 1; i < n; i++) {

        for (let k = 0; k < n - i; k++) {
            result += `-`;
        }

        for (let j = n; j > n - i; j--) {
            result += `${count}`;
            count++;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    for (let i = n; i >= 1; i--) {

        for (let k = 0; k < n - i; k++) {
            result += `_`;
        }

        for (let j = n; j > n - i; j--) {
            result += `${count}`;
            count++;
        }
        resultSum += result + `\n`;
        result = ``;
    }

    alert(resultSum);
}
//   draw20(prompt()); // Start Function

//////////////////////////////////////////////////////////////
/**
 * 21
 */

const draw21 = (n) => {
    n = Math.floor(n);
    let result = ``;
    let resultSum = ``;
    let count = 0;

    for (let i = 1; i <= n; i++) {
        for (let j = 1; j <= n - 1; j++) {
            result += `__`;
        }

        while (count != 2 * i - 1) {
            result += `*_`;
            count++;
        }

        resultSum += result + `\n`;
    }

    alert(resultSum);
}
// draw21(prompt()); // Start Function

 //////////////////////////////////////////////////////////////
/**
 *
 */

 //////////////////////////////////////////////////////////////