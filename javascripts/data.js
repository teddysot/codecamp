const bills = [
  {
    "id": "1",
    "transactionDate": "2020-08-01",
    "total": 12345,
    "location": "Chonburi",
    "paymentType": "Cash",
    "member": {
      "name": "Tle",
      "age": "26"
    },
    "pointRate": 0.01
  },
  {
    "id": "2",
    "transactionDate": "2020-08-01",
    "total": 12298,
    "location": "Chonburi",
    "paymentType": "Cash",
    "member": {
      "name": "Tle",
      "age": "26"
    },
    "pointRate": 0.01
  },
  {
    "id": "3",
    "transactionDate": "2020-08-01",
    "total": 41012,
    "location": "Suphanburi",
    "paymentType": "MasterCard",
    "member": {
      "name": "Peter",
      "age": 33
    },
    "pointRate": 0.01
  },
  {
    "id": "4",
    "transactionDate": "2020-08-02",
    "total": 24826,
    "location": "Trang",
    "paymentType": "MasterCard",
    "member": {
      "name": "Ball",
      "age": 31
    },
    "pointRate": 0.01
  },
  {
    "id": "5",
    "transactionDate": "2020-08-21",
    "total": 47202,
    "location": "Trat",
    "paymentType": "VISA",
    "member": null
  },
  {
    "id": "6",
    "transactionDate": "2020-08-15",
    "total": 29815,
    "location": "Lopburi",
    "paymentType": "VISA",
    "member": {
      "name": "Tle",
      "age": 26
    },
    "pointRate": 0.01
  },
  {
    "id": "7",
    "transactionDate": "2020-08-14",
    "total": 28375,
    "location": "Chonburi",
    "paymentType": "VISA",
    "member": {
      "name": "Jak",
      "age": 36
    },
    "pointRate": 0.01
  },
  {
    "id": "8",
    "transactionDate": "2020-08-19",
    "total": 26923,
    "location": "Chiang Mai",
    "paymentType": "QR",
    "member": null
  },
  {
    "id": "9",
    "transactionDate": "2020-08-11",
    "total": 12545,
    "location": "Lampang",
    "paymentType": "VISA",
    "member": null
  },
  {
    "id": "10",
    "transactionDate": "2020-08-07",
    "total": 46169,
    "location": "Phuket",
    "paymentType": "MasterCard",
    "member": {
      "name": "Por",
      "age": 28
    },
    "pointRate": 0.01
  },
  {
    "id": "11",
    "transactionDate": "2020-08-11",
    "total": 23655,
    "location": "Saraburi",
    "paymentType": "AliPay",
    "member": {
      "name": "Jit",
      "age": 32
    },
    "pointRate": 0.01
  },
  {
    "id": "12",
    "transactionDate": "2020-08-03",
    "total": 42505,
    "location": "Phuket",
    "paymentType": "QR",
    "member": {
      "name": "Jit",
      "age": 32
    },
    "pointRate": 0.01
  },
  {
    "id": "13",
    "transactionDate": "2020-08-03",
    "total": 15678,
    "location": "Phrae",
    "paymentType": "Cash",
    "member": {
      "name": "Ball",
      "age": 31
    },
    "pointRate": 0.01
  },
  {
    "id": "14",
    "transactionDate": "2020-08-26",
    "total": 53209,
    "location": "Loei",
    "paymentType": "MasterCard",
    "member": {
      "name": "Jak",
      "age": 36
    },
    "pointRate": 0.01
  },
  {
    "id": "15",
    "transactionDate": "2020-08-23",
    "total": 11230,
    "location": "Chiang Rai",
    "paymentType": "Cash",
    "member": null
  },
  {
    "id": "16",
    "transactionDate": "2020-08-26",
    "total": 26748,
    "location": "Nakhon Pathom",
    "paymentType": "Cash",
    "member": null
  },
  {
    "id": "17",
    "transactionDate": "2020-08-28",
    "total": 15885,
    "location": "Bangkok",
    "paymentType": "MasterCard",
    "member": {
      "name": "Tawan",
      "age": 29
    },
    "pointRate": 0.01
  },
  {
    "id": "18",
    "transactionDate": "2020-08-02",
    "total": 38590,
    "location": "Samut Prakan",
    "paymentType": "AliPay",
    "member": {
      "name": "Pup",
      "age": 38
    },
    "pointRate": 0.01
  },
  {
    "id": "19",
    "transactionDate": "2020-08-17",
    "total": 35786,
    "location": "Bangkok",
    "paymentType": "VISA",
    "member": {
      "name": "Ohm",
      "age": 21
    },
    "pointRate": 0.01
  },
  {
    "id": "20",
    "transactionDate": "2020-08-04",
    "total": 23085,
    "location": "Bangkok",
    "paymentType": "VISA",
    "member": null
  },
  {
    "id": "21",
    "transactionDate": "2020-08-03",
    "total": 49957,
    "location": "Nakhon Pathom",
    "paymentType": "MasterCard",
    "member": null
  },
  {
    "id": "22",
    "transactionDate": "2020-08-18",
    "total": 38872,
    "location": "Chonburi",
    "paymentType": "VISA",
    "member": null
  },
  {
    "id": "23",
    "transactionDate": "2020-08-29",
    "total": 127142,
    "location": "Chonburi",
    "paymentType": "Cash",
    "member": {
      "name": "Prince",
      "age": 27
    },
    "pointRate": 0.01
  },
  {
    "id": "24",
    "transactionDate": "2020-08-05",
    "total": 40543,
    "location": "Rayong",
    "paymentType": "Cash",
    "member": null
  },
  {
    "id": "25",
    "transactionDate": "2020-08-21",
    "total": 11315,
    "location": "Yala",
    "paymentType": "VISA",
    "member": {
      "name": "Bank",
      "age": 40
    },
    "pointRate": 0.01
  },
  {
    "id": "26",
    "transactionDate": "2020-08-08",
    "total": 42612,
    "location": "Ranong",
    "paymentType": "Cash",
    "member": null
  },
  {
    "id": "27",
    "transactionDate": "2020-08-11",
    "total": 21988,
    "location": "Chiang Mai",
    "paymentType": "AliPay",
    "member": null
  },
  {
    "id": "28",
    "transactionDate": "2020-08-13",
    "total": 48147,
    "location": "Phayao",
    "paymentType": "AliPay",
    "member": null
  },
  {
    "id": "29",
    "transactionDate": "2020-08-23",
    "total": 23548,
    "location": "Loei",
    "paymentType": "MasterCard",
    "member": null
  },
  {
    "id": "30",
    "transactionDate": "2020-08-27",
    "total": 23218,
    "location": "Khonkaen",
    "paymentType": "VISA",
    "member": null
  },
  {
    "id": "31",
    "transactionDate": "2020-08-24",
    "total": 37463,
    "location": "Nan",
    "paymentType": "AliPay",
    "member": {
      "name": "Tle",
      "age": 26
    },
    "pointRate": 0.01
  },
  {
    "id": "32",
    "transactionDate": "2020-08-06",
    "total": 27477,
    "location": "Bangkok",
    "paymentType": "MasterCard",
    "member": {
      "name": "P",
      "age": 49
    },
    "pointRate": 0.01
  },
  {
    "id": "33",
    "transactionDate": "2020-08-22",
    "total": 109872,
    "location": "Bangkok",
    "paymentType": "VISA",
    "member": {
      "name": "Tle",
      "age": 26
    },
    "pointRate": 0.02
  },
  {
    "id": "34",
    "transactionDate": "2020-08-06",
    "total": 37786,
    "location": "Bangkok",
    "paymentType": "Cash",
    "member": null
  },
  {
    "id": "35",
    "transactionDate": "2020-08-10",
    "total": 120286,
    "location": "Bangkok",
    "paymentType": "VISA",
    "member": {
      "name": "Ball",
      "age": 31
    },
    "pointRate": 0.02
  },
  {
    "id": "36",
    "transactionDate": "2020-08-25",
    "total": 74321,
    "location": "Nakhon Sawan",
    "paymentType": "QR",
    "member": {
      "name": "Tle",
      "age": 26
    },
    "pointRate": 0.01
  }
];

/**
 * 1. มีทั้งหมดกี่บิล
 * 2. หายอดรวมTotal
 * 3. หาaverage total
 * 4. หาจำนวน member ทั้งหมด
 * 5. หาจำนวน total แต่ละ location
 * 6. หาจำนวน total แต่ละ payment
 * 7. หายอด total ของ member แต่ละคน
 * 8. หา total ของแต่ละวัน
 * 9. ในแต่ละ location หา total sale, bill, average total จำแนกตาม transaction type
 */

const countBill = bills.length;
const totalBill = bills.reduce((acc, item) => {
  return acc + item.total;
}, 0)
const avgTotal = totalBill / countBill;

const countMember = () => {
  let memberSet = new Set();
  for (const it1 of bills) {
    if (it1.member != null) {
      memberSet.add(it1.member.name)
    }
  }
  console.log(`Total Member:${memberSet.size}`);
};

const totalOfLocation = () => {
  let locationSet = new Set();
  let mapLocation = {};
  for (const it1 of bills) {
    if (it1.location != null) {
      locationSet.add(it1.location)
    }
  }
  for (const iterator of locationSet) {
    const countTotal = bills.reduce((acc, item) => {
      if (item.location == iterator) {
        return acc + item.total;
      }
      return acc + 0;
    }, 0)
    mapLocation[iterator] = countTotal;
  }
  let ordered = {}
  Object.keys(mapLocation).sort().forEach(function (key) {
    ordered[key] = mapLocation[key];
  });

  console.log(ordered);
}

const totalOfPayment = () => {
  let paymentSet = new Set();
  let paymentMap = {}
  for (const it1 of bills) {
    if (it1.paymentType != null) {
      paymentSet.add(it1.paymentType);
    }
  }

  for (const iterator of paymentSet) {
    const countTotal = bills.reduce((acc, item) => {
      if (item.paymentType == iterator) {
        return acc + item.total;
      }
      return acc + 0;
    }, 0)
    paymentMap[iterator] = countTotal;
  }
  let ordered = {}
  Object.keys(paymentMap).sort().forEach(function (key) {
    ordered[key] = paymentMap[key];
  });

  console.log(ordered);
}

const totalOfMember = () => {
  let memberSet = new Set();
  let memberMap = {}
  for (const it1 of bills) {
    if (it1.member == null) {
      continue;
    }
    if (it1.member != null) {
      memberSet.add(it1.member.name)
    }
  }
  for (const iterator of memberSet) {
    const countTotal = bills.reduce((acc, item) => {
      if (item.member == null) {
        return acc + 0;
      }
      if (item.member.name == iterator) {
        return acc + item.total;
      }
      return acc + 0;
    }, 0)
    memberMap[iterator] = countTotal;
  }
  let ordered = {}
  Object.keys(memberMap).sort().forEach(function (key) {
    ordered[key] = memberMap[key];
  });

  console.log(ordered);
}

const totalOfDate = () => {
  let dateSet = new Set();
  let dateMap = {}
  for (const it1 of bills) {
    if (it1.transactionDate == null) {
      continue;
    }
    if (it1.transactionDate != null) {
      dateSet.add(it1.transactionDate)
    }
  }
  for (const iterator of dateSet) {
    const countTotal = bills.reduce((acc, item) => {
      if (item.transactionDate == null) {
        return acc + 0;
      }
      if (item.transactionDate == iterator) {
        return acc + item.total;
      }
      return acc + 0;
    }, 0)
    dateMap[iterator] = countTotal;
  }
  let ordered = {}
  Object.keys(dateMap).sort().forEach(function (key) {
    ordered[key] = dateMap[key];
  });

  console.log(ordered);
}


const totalOfAll = () => {
  let paymentSet = new Set();
  for (const it of bills) {
    if (it.paymentType != null) {
      paymentSet.add(it.paymentType);
    }
  }

  let locationSet = new Set();
  for (const it of bills) {
    if (it.location != null) {
      locationSet.add(it.location)
    }
  }
  let locationMap = {}
  for (const iterator of locationSet) {
    locationMap[iterator] = {}
    for (const iterator2 of paymentSet) {
      locationMap[iterator][iterator2] = { 'Total': 0, 'Bill': 0, 'Average': 0 }
    }
  }

  for (const iterator of bills) {
    if (iterator.location != null) {
      if (locationMap[iterator.location][iterator.paymentType] != null) {
        locationMap[iterator.location][iterator.paymentType].Total += iterator.total;
        locationMap[iterator.location][iterator.paymentType].Bill++;
        locationMap[iterator.location][iterator.paymentType].Average = locationMap[iterator.location][iterator.paymentType].Total / locationMap[iterator.location][iterator.paymentType].Bill;
      }
    }
  }
  let ordered = {}
  Object.keys(locationMap).sort().forEach(function (key) {
    ordered[key] = locationMap[key];
  });

  console.log(ordered);
}

const totalInAll = () => {
  const locationFilter = bills.reduce((acc, bill) => {
    acc[bill.location] = bills.reduce((_acc, _bill) => {

      if (_acc[_bill.paymentType]) {
        if (bill.location == _bill.location) {
          _acc[_bill.paymentType].total = _acc[_bill.paymentType].total + _bill.total;
          _acc[_bill.paymentType].bill = _acc[_bill.paymentType].bill + 1;
          _acc[_bill.paymentType].average = _acc[_bill.paymentType].total / _acc[_bill.paymentType].bill;
        }
      }
      else {
        _acc[_bill.paymentType] = {
          total: 0,
          bill: 0,
          average: 0,
        }
      }
      return _acc;
    }, {});
    return acc;
  }, {})

  console.log(locationFilter);
}

// console.log(countBill);
// console.log(totalBill);
// console.log(avgTotal);
// countMember();
// totalOfPayment();
// totalOfLocation();
// totalOfMember();
// totalOfDate();
// totalOfAll();
totalInAll();