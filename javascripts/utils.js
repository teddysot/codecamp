const sumAll = (...args) => {
    let sum = 0;

    for (const iterator of args) {
        sum += iterator;
    }

    return sum;
}

// console.log(sumAll(1,2,3));

const mergeChar = (...args) => {
    let chars = ''

    for (const iterator of args) {
        chars += String(iterator);
    }

    return chars;
}

// console.log(mergeChar('C', 'a', 't'));

const showName = (firstName, lastName, ...titles) => {
    console.log(`${firstName} ${lastName}`); // Julius Caesar

    // ที่เหลือ (the rest) กลายเป็น array ชื่อ titles
    // เช่น titles = ["Consul", "Imperator"]

    console.log(titles[0]);
    console.log(titles[1]);
    console.log(titles.length);
}

// showName('Julius', "Caesar", "Consul", "Imperator");

const user = {
    name: "John",
    years: 30,
};

const { name: name, years: age, isAdmin: isAdmin = false } = user;

// console.log(isAdmin);
// console.log(name);
// console.log(age);
// console.log(user);

const salaries = {
    "John": 100,
    "Pete": 300,
    "Mary": 250
};

let { John, Pete, Mary } = salaries;


const topSalary = () => {
    let max = Number.NEGATIVE_INFINITY
    let name = null
    for (const key in salaries) {
        if (salaries[key] > max) {
            max = salaries[key]
            name = key;
        }
    }

    console.log(name);
}

// topSalary();

let animal = {
    eats: true
}

let rabbit = {
    jumps: true,
    __proto__: animal
}

class User {
    constructor() {
        this.name = "name"
        this.age = "age"
    }
}

class Admin extends User {
    constructor() {
        super();
        this.permission = [];
    }
}

let result = 0
for (let i = 1; i <= 1452; i+=2) {
    result = result + (i * (i + 1));
}

console.log(result);