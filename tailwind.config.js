module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      colors: {
        'regal-blue': '#243c5a',
      }
    },
  },
  variants: {},
  plugins: [],
}
